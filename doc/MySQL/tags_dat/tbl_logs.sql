-- 行为日志表：tbl_logs
--    电商网站中用户浏览网站访问行为日志数据（浏览数据），总共11个字段，此类数据属于最多。
--    376983条

CREATE TABLE `tbl_logs` (
`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
`log_id` varchar(50) DEFAULT NULL,
`remote_ip` varchar(50) DEFAULT NULL,
`site_global_ticket` varchar(250) DEFAULT NULL,
`site_global_session` varchar(250) DEFAULT NULL,
`global_user_id` varchar(50) DEFAULT NULL,
`cookie_text` mediumtext,
`user_agent` varchar(250) DEFAULT NULL,
`ref_url` varchar(250) DEFAULT NULL,
`loc_url` varchar(250) DEFAULT NULL,
`log_time` varchar(50) DEFAULT NULL,
PRIMARY KEY (`id`),
KEY `log_time` (`log_time`)
) ENGINE=MyISAM AUTO_INCREMENT=1160286 DEFAULT CHARSET=utf8;
#!/bin/bash

jars=''
for file in `\ls  /usr/hdp/3.1.4.0-315/hbase/lib/hbase*`
do
  jars+=$file","
done
echo $jars

jars+="tag-etl-1.0-SNAPSHOT.jar"


tableName=$1

/usr/hdp/3.1.4.0-315/spark2/bin/spark-submit \
--jars $jars \
--master yarn \
--class com.chb.tags.etl.hfile.HBaseBulkLoader \
tag-etl-1.0-SNAPSHOT.jar 1 $tableName detail /warehouse/tablespace/managed/hive/tags_dat.db/$tableName /datas/output_hfile/$tableName
#!/bin/bash
source db.sh
# tableName="tbl_users"
tableName=$1
sqoop create-hive-table \
--connect jdbc:mysql://$host:3306/$db \
--table $tableName \
--username $user \
--password $pwd \
--hive-database $db \
--hive-table $tableName \
--fields-terminated-by '\t' \
--lines-terminated-by '\n'